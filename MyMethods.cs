using System;
namespace String
{
    public class MyMethods
    {
        public static void SwitchMethod(){
            Console.WriteLine("---Welcome to String task! \n 1.Convert To Lowercase, \n 2.Find a Longest Word, \n 3.Reverse the Sentence, \n 4.Reverse Order Letter,\n 5.Check Whether an Alphabet,----------");
            Console.WriteLine("Choose Which task Would you like to Run from the Above Tasks:");
            int taskNumber =Convert.ToInt32(Console.ReadLine());
              switch(taskNumber){
            case 1:
                 ConvertToLower();
            break;
            case 2:
                FindLongestWord();
            break;
            case 3:
              ReverseTheSentence();
            break;
            case 4:
               ReverseOrderletter();
            break;
            case 5:
                CheckAlphabet();
            break;
            default:
            Console.WriteLine("Error you have given a invalid task number as input");
            break;
              }
            Console.WriteLine("Would you like to do String task again (Y/N)?:");
            string TaskYesOrNo = Console.ReadLine().ToUpper();
            if(TaskYesOrNo=="Y"){
            SwitchLoop(TaskYesOrNo);
            }
            TaskYesOrNo="";
        }

        public static void SwitchLoop(string TaskYesOrNo){
            while(TaskYesOrNo=="Y"){
                SwitchMethod();
                TaskYesOrNo="";
            }
        }

        public static void ConvertToLower(){
            Console.WriteLine("------------Task-1 Convert To LowerCase--------");
            Console.WriteLine("Enter Some Words in Captial and Small Letter:");
            string data =Console.ReadLine().ToLower();
            Console.WriteLine("Output:");
            Console.WriteLine("Your data to LowerCase: "+data);
            Console.WriteLine("---------------------------");
        }
        public static void FindLongestWord(){
            Console.WriteLine("------------Task-2 Find a Longest Word--------");
            Console.WriteLine("Enter Some Words in Sentence:");
            string line = Console.ReadLine();
           
                    string[] words = line.Split(" ");
                        string word = "";
                        int ctr = 0;
                        foreach (string s in words)
                        {
                            if (s.Length > ctr)
                            {
                                word = s;
                                ctr = s.Length;
                            }
                        }
                    Console.WriteLine("Output:");
                    Console.WriteLine(word);
                    Console.WriteLine("---------------------------");
        }
        public static void ReverseTheSentence(){
            Console.WriteLine("------------Task-3 Reverse The Sentence--------");
            Console.WriteLine("Enter Some Words in Sentence:");
            string data =Console.ReadLine();
            string [] reverseWord = data.Split(' ');

             Array.Reverse(reverseWord);
            Console.WriteLine("Output:");
             Console.WriteLine("Original String is:\n"+data);
             Console.WriteLine("Reverse String is:");

             for(int i=0;i<reverseWord.Length;i++)

                {
                Console.Write(reverseWord[i]+""+' ');
                }
                Console.WriteLine("\n---------------------------");
        }
        public static void ReverseOrderletter(){
            Console.WriteLine("------------Task-4 Reverse Order the letter--------");
            
            char letter,letter1,letter2;
  
            Console.Write("Input letter1: ");
            letter = Convert.ToChar(Console.ReadLine());
        
            Console.Write("Input letter2: ");
            letter1 = Convert.ToChar(Console.ReadLine());       
        
            Console.Write("Input letter3: ");
            letter2 = Convert.ToChar(Console.ReadLine());

            Console.WriteLine("Output:");
            Console.WriteLine("{0} {1} {2}",letter2,letter1,letter);
            Console.WriteLine("---------------------------");
        }
        public static void CheckAlphabet(){
            Console.WriteLine("------------Task-5 Check the Alphabet is Vowel Or Consonant--------");
            
            char character;
        Console.WriteLine("Enter an alphabet: ");
        character = Char.Parse(Console.ReadLine());
        Console.WriteLine("Output:");
        if((character >= 'A' && character <= 'Z')||character >= 'a' && character <= 'z')
        {
            if (character == 'a' || character == 'A' || character == 'e' || character == 'E' || character == 'i'
                || character == 'I' || character == 'o' || character == 'O' || character == 'u' || character == 'U')
            {
                Console.WriteLine(character + " is a vowel");
            }
            else
            {
                Console.WriteLine(character + " is a consonant");
            }
        }
        else
        {
            Console.WriteLine(character + " is not an alphabet");
        }
         Console.WriteLine("---------------------------");
        }
    }
}